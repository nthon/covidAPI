# COVID API
API for live information about COVID-19

GET https://covid19data.dev/api/all -> global info

GET https://covid19data.dev/api/countries -> all countries info

GET https://covid19data.dev/api/countries/{countryName} -> country specific information

**The api part is a fork from https://github.com/javieraviles/covidAPI

an ionic frontend is showing the info nicely at https://covid19data.dev/
